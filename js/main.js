$(document).ready(function(){
	var countSlide = $(".screens").length;
	countSlide = countSlide - 1;

	$(".slide_right").click(function(){
		$(".currentslide").removeClass("currentslide").next(".screens").addClass("currentslide");

		$(".timeline_window").animate({ left: "-=530px" },"slow","easeOutBack", function(){
			// var position = $(this).position();
			var currentIndex = $(".currentslide").index();
			if (currentIndex > 0) {
				$(".slide_left").show();
			} else {
				$(".slide_left").hide();
			}

			if (currentIndex === countSlide) {
				$(".slide_right").hide();
			} else {
				$(".slide_right").show();
			}
		});
		return false;
	});

	$(".slide_left").click(function(){
		$(".currentslide").removeClass("currentslide").prev(".screens").addClass("currentslide");
		$(".timeline_window").animate({ left: "+=530px" },"slow","easeOutBack", function(){
			// var position = $(this).position();
			var currentIndex = $(".currentslide").index();
			if (currentIndex > 0) {
				$(".slide_left").show();
			} else {
				$(".slide_left").hide();
			}

			if (currentIndex === countSlide) {
				$(".slide_right").hide();
			} else {
				$(".slide_right").show();
			}
		});
		return false;
	});
});