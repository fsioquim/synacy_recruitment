<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Synacy Recruitment</title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
<link rel="stylesheet" type="text/css" media="print" href="css/print.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=PT Sans:normal,bold">
</head>

<body class="home">

  <!-- Why Synacy? -->
  <section id="intro" class="topics">
      <article>
        <header>
          <h1>Do You Want to <span class="heavy">Kick-Ass?</span></h1>
        </header>
        <span class="heavy title">Why Synacy?</span>
        <p>
          Effort is directionally proportional to reward. <br />
          Work in an environment that rewards initiative, hard work, and ingenuity.
        </p>
        <p>
          Get the tools need for success <br />
          We'll orivude the space and environment you need to work efficiently.
        </p>
        <p>
          Learn from the technology trainer of top Philippine companies <br />
          Get screened and trained by Synacy's training partner, O&B Software Labs.
        </p>
      </article>
  </section>

  <!-- Timeline Slider -->
  <section id="timeline" class="topics">
      <div class="timeline_window">
        <article id="screen1" class="screens currentslide">
          <div class="circle_container">
            <div class="front_info">
              <header>
                <h1>WHO WE ARE</h1>
              </header>
              <p>
                We're a young, innovative company, and we're looking for new talent to join us
                in Cebu. We're a technology development company that builds product and support
                services. Our software development projects provide interesting technical challenges
                - including scaling a high volume transactional system, optimising and building
                APIs for our partners, and developing exciting front end products. We also have a
                few ideas that will keep things interesting. We employ agile development methods
                utilising Java, Grails, Spring, PostgreSQL and LAMP.
              </p>
            </div>
            <div class="back_info">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, rem, eligendi modi error perferendis vitae commodi tenetur cupiditate eius dolor aspernatur cum veritatis obcaecati debitis excepturi quod eum culpa ad! </p>
            </div>
          </div>
        </article>
        <article id="screen2" class="screens">
          <div class="circle_container">
            <div class="front_info">
              <img src="<?php bloginfo('template_directory'); ?>/images/chaticon.png" alt="Sponsor Cebu SW2" title="Sponsor Cebu SW2" />
              <header>
                <h1>Mentor Cebu SW2</h1>
              </header>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, voluptatum dolore quo beatae laboriosam eos totam quas cum esse quam?
              </p>
               <header>
                <h1>Partnership with Q&B</h1>
              </header>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, eligendi, beatae alias delectus impedit perspiciatis aliquam sit doloremque dolore iure. 
              </p>
            </div>
            <div class="back_info">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, rem, eligendi modi error perferendis vitae commodi tenetur cupiditate eius dolor aspernatur cum veritatis obcaecati debitis excepturi quod eum culpa ad! </p>
            </div>
          </div>
          <strong>November 2012</strong>
        </article>
        <article id="screen3" class="screens">
          <div class="circle_container">
            <div class="front_info">
              <img src="<?php bloginfo('template_directory'); ?>/images/wrenchicon.png" alt="Start Office Build / Commission Kitchen" title="Start Office Build / Commission Kitchen" />
              <header>
                <h1>Start Office Build / Commission Kitchen</h1>
              </header>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam, libero, cumque, ad in ipsum aliquam illum deleniti nulla sint ducimus dolorem incidunt! Praesentium, id aut quibusdam fuga explicabo aliquid a.
              </p>
            </div>
            <div class="back_info">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, rem, eligendi modi error perferendis vitae commodi tenetur cupiditate eius dolor aspernatur cum veritatis obcaecati debitis excepturi quod eum culpa ad! </p>
            </div>
          </div>
          <strong>January 2013</strong>
        </article>
        <article id="screen4" class="screens">
          <div class="circle_container">
            <div class="front_info">
              <img src="<?php bloginfo('template_directory'); ?>/images/chaticon.png" alt="Sponsor Cebu SW3" title="Sponsor Cebu SW3" />
              <header>
                <h1>Sponsor Cebu SW3</h1>
              </header>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, nisi qui saepe quod consequuntur nemo nostrum sapiente a dignissimos neque soluta iure quisquam illo laudantium doloribus quaerat deserunt excepturi? Quas.
              </p>
            </div>
            <div class="back_info">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, rem, eligendi modi error perferendis vitae commodi tenetur cupiditate eius dolor aspernatur cum veritatis obcaecati debitis excepturi quod eum culpa ad! </p>
            </div>
          </div>
           <strong>June 2013</strong>
        </article>
        <article id="screen5" class="screens">
          <div class="circle_container">
            <div class="front_info">
              <img src="<?php bloginfo('template_directory'); ?>/images/idicon.png" alt="First 20 Staff" title="First 20 Staff" />
              <header>
                <h1>First 20 Staff</h1>
              </header>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, nisi qui saepe quod consequuntur nemo nostrum sapiente a dignissimos neque soluta iure quisquam illo laudantium doloribus quaerat deserunt excepturi? Quas.
              </p>
            </div>
            <div class="back_info">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, rem, eligendi modi error perferendis vitae commodi tenetur cupiditate eius dolor aspernatur cum veritatis obcaecati debitis excepturi quod eum culpa ad! </p>
            </div>
          </div>
           <strong>July 2013</strong>
        </article>
        <article id="screen6" class="screens">
          <div class="circle_container">
            <div class="front_info">
              <img src="<?php bloginfo('template_directory'); ?>/images/staricon.png" alt="Making Waves - Featured in Sunstar" title="Making Waves - Featured in Sunstar" />
              <header>
                <h1>Making Waves - Featured in Sunstar</h1>
              </header>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, nisi qui saepe quod consequuntur nemo nostrum sapiente a dignissimos neque soluta iure quisquam illo laudantium doloribus quaerat deserunt excepturi? Quas.
              </p>
            </div>
            <div class="back_info">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, rem, eligendi modi error perferendis vitae commodi tenetur cupiditate eius dolor aspernatur cum veritatis obcaecati debitis excepturi quod eum culpa ad! </p>
            </div>
          </div>
          <strong>July 2013</strong>
        </article>
        <article id="screen7" class="screens">
          <div class="circle_container">
            <div class="front_info">
              <img src="<?php bloginfo('template_directory'); ?>/images/staricon.png" alt="Synacy Graduate Program (Engineering) Introduced" title="Synacy Graduate Program (Engineering) Introduced" />
              <header>
                <h1>Synacy Graduate Program (Engineering) Introduced</h1>
              </header>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, nisi qui saepe quod consequuntur nemo nostrum sapiente a dignissimos neque soluta iure quisquam illo laudantium doloribus quaerat deserunt excepturi? Quas.
              </p>
            </div>
            <div class="back_info">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, rem, eligendi modi error perferendis vitae commodi tenetur cupiditate eius dolor aspernatur cum veritatis obcaecati debitis excepturi quod eum culpa ad! </p>
            </div>
          </div>
          <strong>October 2013</strong>
        </article>
      </div>
      <a href="#" class="slide_right controls">Slide Right</a>
      <a href="#" class="slide_left controls">Slide Left</a>
      <!-- <div id="time-meter"></div> -->
  </section>

  <!-- Our Challenge -->
  <section id="challenge" class="topics">

      <article id="screen1">
        <header>
          <h1>YOUR IMPACT</h1>
        </header>
        <ul>
        <li><span>building solutions to business problems</span></li>
        <li><span>learning from and teaching to your teammates</span></li>
        <li><span>using your brain to make something good, great</span></li>
        <li><span>being positive and friendly</span></li>
        </ul>
      </article>

      <article id="screen2">
        <header>
          <h1>OUR CHALLENGE</h1>
        </header>
        <ul>
        <li><span>solving huge transactional messaging problems</span></li>
        <li><span>scaling and keeping available our international network</span></li>
        <li><span>optimising and building APIs for our partners</span></li>
        <li><span>developing exciting front end products for our end-users</span></li>
        <li><span>we constantly have new ideas to keep things interesting</span></li>
        </ul>
      </article>

  </section>

  <!-- Our Technology -->
  <section id="technology" class="topics">
      <article id="screen1">
        <header>
          <h1>Our Technology Stack</h1>
        </header>
        <ul>
        <li class="left"><span>Java</span></li>
        <li class="right"><span>Spring</span></li>
        <li class="left"><span>Hibernate</span></li>
        <li class="right"><span>PostgreSQL</span></li>
        <li class="li-clear left"><span>Groovy</span></li>
        <li class="right"><span>Grails</span></li>
        <li class="left"><span>LAMP</span></li>
        <li class="right"><span>CentOS</span></li>
        </ul>
      </article>
  </section>

  <!-- The Program -->
  <section id="program" class="topics">
      <article id="screen1">
      	<img src="wp-content/themes/twentythirteen/images/steps.png" />
      	<div id="steps">
					<div id="step1" class="step">
						<span class="step-number">1</span>
						<span class="step-title">FIRST ROUND RECRUITMENT</span>
						<p>We'll look at your resume, your application answers and response to try
						to come up with a short list. Make sure you put your best foot forward and
						try hardest on the first round.</p>
					</div>
					<div id="step2" class="step">
						<span class="step-number">2</span>
						<span class="step-title">TRAINING OFFER MADE</span>
						<p>Short listing begins. To be progressed to this stage means you've got
						something but we need to know more about you. We'll ask you questions, you may
						have multiple interviews and you'll definitely be given coding tasks.</p>
					</div>
					<div id="step3" class="step">
						<span class="step-number">3</span>
						<span class="step-title">MANILA: JAVA BOOTCAMP FUNDAMENTALS</span>
						<p>If you've made it to this far - great, you've been selected to fly to Manila
						and dig into the concept of object, classes and the Java programming language.
						You'll do this  intensively for 10 days.</p>
					</div>
					<div id="step4" class="step">
						<span class="step-number">4</span>
						<span class="step-title">BOOTCAMP WEB</span>
						<p>If you make it to this stage, you'll be taught how to build enterprise-level,
						well-tested web applications while taking into consideration security and performance
						issues. Remeber what you learn here- even if you don't make it it'll be useful.	</p>
					</div>
					<div id="step5" class="step">
						<span class="step-number">5</span>
						<span class="step-title">BOOTCAMP FRAMEWORKS</span>
						<p>Spring, Hibernate, Grails. You'll need to learn some frameworks to be really
						effective. Here you'll be taught the specifics required by Synacy projects.</p>
					</div>
					<div id="step6" class="step">
						<span class="step-number">6</span>
						<span class="step-title">LEARNING SYNACY PROJECTS</span>
						<p>After completion on the boot camp, you'll come to Cebu to debrief. Go over your
						lessons and settle info the office. It's not over yet as now you wil lbe rotated
						to your team. Your team leaders will take over and you'll start to be immersed into
						the Synacy project streams.</p>
					</div>
					<div id="step7" class="step">
						<span class="step-number">7</span>
						<span class="step-title">JOB OFFER</span>
						<p>Rotating through our projects streams will be seen to be productinve, intelligent
						and a team player. You'll one day may even be the team leader who will be taking on the
						next lot of recruits and sharing your wisdom. If you've made it this far you'll get
						the coveted Synacy business card and an appointment. Congratulations.</p>
					</div>
      	</div>
      </article>
  </section>

  <!-- What You Get -->
  <section id="gallery" class="topics">
      <article id="screen1">
      	<img src="wp-content/themes/twentythirteen/images/photos.png" />
      </article>
  </section>

  <!-- Who We're Looking For -->
  <section id="looking" class="topics">
      <article id="screen1">
      	<span id="red" class="speeches">
      		<p><span class="heavy">Entry level</span>: understanding of OOP concepts and Java.</p>
      		<p><span class="heavy">Bonuys Points</span>: J2EE, Hibernate, JSON, Selenium, Tomcat, JBoss, JIRA, SPring, JSP, JSF, Struts,
      		AJAX, PL/SQL, etc.</p>
      		<p><span class="heavy">Also, past experiences in being a Software Engineer would be a great help since they are already exposed
      		to it.</span></p>
      		<p><span class="heavy">Any certifications</span>: Oracle Certified Professional, Java Programmer (OCP-JP) <br />
      			Oracle Advanced PL/SQL Developer Certified Professional (OCP) <br />
      			(Associate Exams) <br />
      			- Oracle Database SQL Expert <br />
      			- Program with PL/SQL <br />
      			- Oracle Forms: Build Internet Applications <br />
      			- Advanced PL/SQL <br />
      			Oracle Certified Professional, Java Web Services Developer (OCP-JWS) <br />
      			Oracle Certified Professional, Web Component Developer (OCP-WCD) <br />
      			Oracle Certified Database Administration (OCDBA) <br />
      			Oracle Certified Master Enterprise Architect (OCM-EA)
      		</p>

      	</span>
      	<span id="orange" class="speeches"></span>
      	<span id="green" class="speeches"></span>
      	<span id="blue" class="speeches"></span>
      </article>
  </section>

  <!-- Application Program -->
  <section id="application" class="topics">
      <article id="screen1">
        <header>
          <h1>Interviewing at Synacy</h1>
        </header>
				<span id="step-number-1"  class="step-number">1</span>
				<span id="step-number-2"  class="step-number">2</span>
				<span id="step-number-3"  class="step-number">3</span>
				<span id="step-number-4"  class="step-number">4</span>
				<span id="step-number-5"  class="step-number">5</span>
				<span id="step-number-6"  class="step-number">6</span>
				<span id="step-number-7"  class="step-number">7</span>
      	<span id="step1" class="step">application received</span>
      	<span id="step2" class="step">resume reviewed</span>
      	<span id="step3" class="step">applications invited to do online tests</span>
      	<span id="step4" class="step">applications invited to do face to face or video conference</span>
      	<span id="step5" class="step">more tests</span>
      	<span id="step6" class="step">another interview</span>
      	<span id="step7" class="step">offer to training</span>
      	<div id="block-info">
      		<p>
      		The exam will be programming, they can use different programming languages (Java, C, C#, etc.). An example of which is below:
      		</p>
      		<p>
      		Task description <br />
      		Write a function: class Solution { public int solution(int[] A); } <br />
      		that, given a non-empty zero-indexed array A of N integers, returns the maximum value from array A: maxP{ A[i] : 0 ≤ i < N } <br />
      		A[3] = 42 A[4] = 1 A[5] = -10 <br />
      		your function should return 42. <br />
      		Assume that: N is an integer within the range [1.. 1,000,000]; <br />
      		each elemetn of array A is an integer within the range [-2,147,483,648..2,147,483,647]. <br />
      		Complexity: expected worst-case is O(1), beyond input storage <br />
      		(not ocunting the storage required for input arguements).
      		</p>
      	</div>
      </article>
  </section>

  <!-- Apply Now -->
  <section id="apply" class="topics">
      <article>
        <header>
          <h1>ARE YOU READY TO</h1>
          <span class="heavy">KICK-ASS?</span>
        	<span class="title">APPLY NOW?</span>
        </header>
      </article>
  </section>


  <footer>
  </footer>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php bloginfo('template_directory'); ?>/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
  <script src="<?php bloginfo('template_directory'); ?>/js/easing.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>

  <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
  <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');
  </script>
  </body>
</html>